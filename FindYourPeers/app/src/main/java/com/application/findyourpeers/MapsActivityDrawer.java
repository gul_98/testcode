package com.application.findyourpeers;

import android.*;
import android.Manifest;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

public class MapsActivityDrawer extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener,
        OnMapReadyCallback,
        ActivityCompat.OnRequestPermissionsResultCallback,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener ,
        LocationListener,
        AdapterView.OnItemClickListener{
    private GoogleMap mMap;
    List<String> array;
    private String email;
    private String uid;
    GoogleApiClient mGoogleApiClient;
    android.location.Location mLastLocation;
    double currentLatitude;
    double currentLongitude;
    Location loc;
    LocationRequest locationrequest;
    ArrayAdapter<String> arrayAdapter;
    Marker mark;
    HashMap<String,Marker> uidToMarker;
    HashMap<String,String> emailToId;
    ImageButton sendrequest;
    EditText requestEmail;
    String UID;

    ListView lv;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps_activity_drawer);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        emailToId=new HashMap<String,String>();
        ///////////////////////Maps Activity///////////////
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map2);
        locationrequest= LocationRequest.create();
        locationrequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationrequest.setInterval(2000);

        mapFragment.getMapAsync(this);
        if (mGoogleApiClient == null) {
            mGoogleApiClient = new GoogleApiClient.Builder(this)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .build();
        }
        SharedPreferences prefs = getSharedPreferences("UID", MODE_PRIVATE);
        uid = prefs.getString("uid", null);
        email=prefs.getString("email",null);
        sendrequest=(ImageButton)findViewById(R.id.send_request);
        requestEmail=(EditText)findViewById(R.id.email_Request);
        sendrequest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                UID="";
                String email= emailParser(requestEmail.getText().toString());
                FirebaseDatabase.getInstance().getReference().child("EmailToId").child(email)
                        .addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(DataSnapshot dataSnapshot) {
                                if(dataSnapshot.exists()){
                                    UID=dataSnapshot.getValue().toString();
                                    FirebaseDatabase.getInstance().getReference().child("UserFriends").child(UID)
                                            .addListenerForSingleValueEvent(new ValueEventListener() {
                                                @Override
                                                public void onDataChange(DataSnapshot dataSnapshot) {
                                                    if(dataSnapshot.exists()){
                                                        String ids=dataSnapshot.getValue().toString();
                                                        ids=ids+uid+" ";
                                                        FirebaseDatabase.getInstance().getReference().child("UserFriends")
                                                                .child(UID).setValue(ids);
                                                        requestEmail.setText("");

                                                    }
                                                }

                                                @Override
                                                public void onCancelled(DatabaseError databaseError) {

                                                }
                                            });
                                }
                            }

                            @Override
                            public void onCancelled(DatabaseError databaseError) {

                            }
                        });

            }
        });

        uidToMarker= new HashMap<String,Marker>();
        int permissionCheck = ContextCompat.checkSelfPermission(this,
                android.Manifest.permission.ACCESS_COARSE_LOCATION);
        int permissionCheck2 = ContextCompat.checkSelfPermission(this,
                android.Manifest.permission.ACCESS_FINE_LOCATION);
        if(permissionCheck== PackageManager.PERMISSION_GRANTED){
            //permission has been granted
        }
        else{
            ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.ACCESS_COARSE_LOCATION},1);
        }
        if(permissionCheck2==PackageManager.PERMISSION_GRANTED){
            //permissionisgranter
        }
        else{
            ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},1);
        }
        /////////////////////////////////////////////////////////////////
        lv=(ListView)findViewById(R.id.ListView);
        lv.setOnItemClickListener(this);
        array= new ArrayList<String>();
        array.add("");
        arrayAdapter = new ArrayAdapter<String>(
                this,
                android.R.layout.simple_list_item_1,
                array );
        lv.setAdapter(arrayAdapter);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.maps_activity_drawer, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            //show pop up dialog
            Intent i= new Intent(this,PeerRequests.class);
            startActivity(i);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_camera) {
            // Handle the camera action
        } else if (id == R.id.nav_gallery) {

        } else if (id == R.id.nav_slideshow) {

        } else if (id == R.id.nav_manage) {

        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_send) {

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

//        List<String> array= new ArrayList<String>();
 //       for(int i = 0 ; i < 10; i++){
 //           array.add(String.valueOf(i));
 //       }
 //       ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(
 //               this,
 //               android.R.layout.simple_list_item_1,
  //              array );
  //      lv.setAdapter(arrayAdapter);

       FirebaseDatabase.getInstance().getReference().child("UserPeers")
               .child(uid).addValueEventListener(new ValueEventListener() {
           @Override
           public void onDataChange(DataSnapshot dataSnapshot) {
               List<String> newArray=new ArrayList<String>();
               if(dataSnapshot.exists()){
                   newArray=parser(dataSnapshot.getValue().toString());
                   for(int i = 0 ; i < newArray.size();i++){
                       FirebaseDatabase.getInstance().getReference().child("Users").child(newArray.get(i))
                               .addListenerForSingleValueEvent(new ValueEventListener() {
                                   @Override
                                   public void onDataChange(DataSnapshot dataSnapshot) {
                                       if(dataSnapshot.exists()){
                                           emailToId.put(reverseparser(dataSnapshot.getValue().toString()),dataSnapshot.getKey());
                                           array.add(reverseparser(dataSnapshot.getValue().toString()));
                                           arrayAdapter.notifyDataSetChanged();
                                       }
                                   }

                                   @Override
                                   public void onCancelled(DatabaseError databaseError) {

                                   }
                               });
                       FirebaseDatabase.getInstance().getReference().child("UserLocation").child(newArray.get(i))
                               .addValueEventListener(new ValueEventListener() {
                                   @Override
                                   public void onDataChange(DataSnapshot dataSnapshot) {
                                       if(dataSnapshot.exists()) {
                                           UserLocation user = (UserLocation) dataSnapshot.getValue(UserLocation.class);
                                           LatLng marker = new LatLng(user.lat, user.lng);
                                           if (mark == null) {
                                               mark = mMap.addMarker(new MarkerOptions().position(marker).title("a friend"));
                                               uidToMarker.put(dataSnapshot.getKey(),mark);
                                           } else {
                                               mark=uidToMarker.get(dataSnapshot.getKey());
                                               mark.setPosition(marker);

                                           }
                                       }
                                   }

                                   @Override
                                   public void onCancelled(DatabaseError databaseError) {

                                   }
                               });
                   }

               }
           }

           @Override
           public void onCancelled(DatabaseError databaseError) {

           }
       });

        // Add a marker in Sydney and move the camera
        if(ContextCompat.checkSelfPermission(this,
                android.Manifest.permission.ACCESS_COARSE_LOCATION)==PackageManager.PERMISSION_GRANTED) {
            mMap.setMyLocationEnabled(true);

        }

        LatLng sydney = new LatLng(-34, 151);
        mLastLocation = LocationServices.FusedLocationApi.getLastLocation(
                mGoogleApiClient);
        if (mLastLocation != null) {
            currentLatitude=mLastLocation.getLatitude();
            currentLongitude=mLastLocation.getLongitude();
            Log.e("location update",String.valueOf(currentLatitude)+"  "+String.valueOf(currentLongitude));

        }
        LatLng myloc=new LatLng(currentLatitude,currentLongitude);
        // mMap.addMarker(new MarkerOptions().position(myloc).title("currentlocation"));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(myloc));

    }
    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 1: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.

                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }
            case 2:{
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.

                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }
    protected void onStart() {
        mGoogleApiClient.connect();
        super.onStart();
    }

    protected void onStop() {
        mGoogleApiClient.disconnect();
        super.onStop();
    }
    @Override
    public void onConnected(Bundle connectionHint) {

        //add a boolean variable if user makes it false it should no more work
        startLocationUpdates();



        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }

        mLastLocation = LocationServices.FusedLocationApi.getLastLocation(
                mGoogleApiClient);
        if (mLastLocation != null) {
            currentLatitude=mLastLocation.getLatitude();
            currentLongitude=mLastLocation.getLongitude();
            LatLng myloc=new LatLng(currentLatitude,currentLongitude);
            //  mMap.addMarker(new MarkerOptions().position(myloc).title("currentlocation"));
            mMap.moveCamera(CameraUpdateFactory.newCameraPosition(
                    new CameraPosition.Builder().
                            target(myloc).
                            zoom((float) 15.5).
                            build()
            ));

            FirebaseDatabase.getInstance().getReference().child("UserLocation").child(uid).
                    setValue(new UserLocation(currentLatitude,currentLongitude, DateFormat.getTimeInstance().format(new Date())));
            Log.e("location update",String.valueOf(currentLatitude)+"  "+String.valueOf(currentLongitude));

        }

    }
    @Override
    public void onConnectionSuspended(int i) {
        Log.v("GoogleApiClient: ", "Connection Suspended. Trying to reconnect");
        mGoogleApiClient.connect();
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.v("GoogleApiClient: ", "Connection Failed. Trying to reconnect");
        mGoogleApiClient.connect();
    }
    @Override
    public void onLocationChanged(Location location) {
        loc = location;
        currentLatitude=loc.getLatitude();
        currentLongitude=loc.getLongitude();
        LatLng myloc=new LatLng(currentLatitude,currentLongitude);
       // mMap.moveCamera(CameraUpdateFactory.newCameraPosition(
         //       new CameraPosition.Builder().
           //             target(myloc).
                        //zoom((float) 15.5).
             //           build()
        //));

        Log.e("location update","hello");
        FirebaseDatabase.getInstance().getReference().child("UserLocation").child(uid).
                setValue(new UserLocation(currentLatitude,currentLongitude,DateFormat.getTimeInstance().format(new Date())));

    }
    @Override
    protected void onPause() {
        super.onPause();
        stopLocationUpdates();
    }

    protected void stopLocationUpdates() {
        LocationServices.FusedLocationApi.removeLocationUpdates(
                mGoogleApiClient, this);
    }

    protected void startLocationUpdates() {
        if(ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)!=PackageManager.PERMISSION_GRANTED){
            Log.e("hello","world");
            return;
        }
        LocationServices.FusedLocationApi.requestLocationUpdates(
                mGoogleApiClient,locationrequest,this);
    }
    public List<String> parser(String UIDS){
        String Uid="";

        List<String> uidlist= new ArrayList<String>();
        for(int i = 0 ; i <UIDS.length();i++){
            if(UIDS.charAt(i)!=' '){
                Uid=Uid+String.valueOf(UIDS.charAt(i));
            }
            else{
                uidlist.add(Uid);
                Uid="";
            }
        }
        return uidlist;
    }
    public String reverseparser(String s){
        String email="";
        for(int i =0; i<s.length();i++){
            if(s.charAt(i)!='!'){
                email=email+String.valueOf(s.charAt(i));
            }
            else{
                email=email+'.';
            }
        }
        return email;
    }
    @Override
    public void onItemClick (AdapterView<?> parent,
                      View view,
                      int position,
                      long id){
        if(array.get(position)!=" ") {
            Marker ClickedMark = uidToMarker.get(emailToId.get(array.get(position)));
            LatLng pos= ClickedMark.getPosition();
            mMap.moveCamera(CameraUpdateFactory.newCameraPosition(
                    new CameraPosition.Builder().
                            target(pos).
                            zoom((float) 15.5).
                            build()
            ));
            Log.e("Location Update", String.valueOf(pos));

        }
    }
    public String emailParser(String s){
        String email="";
        for(int i = 0 ; i < s.length(); i ++){
            if(s.charAt(i)!='.'){
                email=email+String.valueOf(s.charAt(i));
            }
            else{
                email=email+"!";
            }
        }
        return email;
    }
}
