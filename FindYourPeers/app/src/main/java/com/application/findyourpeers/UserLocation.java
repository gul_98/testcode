package com.application.findyourpeers;

import java.util.Date;

/**
 * Created by gul on 7/2/16.
 */
public class UserLocation {
    public double lat;
    public double lng;
    public String date;
    public UserLocation(){}
    public UserLocation(double lat, double lng, String date){
        this.lat=lat;
        this.lng=lng;
        this.date=date;
    }
}
