package com.application.findyourpeers;

import android.content.SharedPreferences;
import android.view.View;
import android.widget.Toast;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.w3c.dom.Text;

import java.util.List;

/**
 * Created by gul on 7/3/16.
 */
public class RequestsAdapater extends ArrayAdapter<String> implements View.OnClickListener {

    Context context;
    List<String> GrabbingData;
    String uid;
    String email;
    String UID;
    String data;


    public RequestsAdapater(Context context, int resourceId,
                            List<String> items) {
        super(context, resourceId, items);
        this.context = context;
        this.GrabbingData= items;
        SharedPreferences prefs = context.getSharedPreferences("UID", context.MODE_PRIVATE);
        uid = prefs.getString("uid", null);
        email=prefs.getString("email",null);


    }


    /*private view holder class*/
    private class ViewHolder {
        TextView Email;
        Button Accept;
        Button Reject;
    }
    public String item;



    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        String rowItem = GrabbingData.get(position);


        item = rowItem;
        LayoutInflater mInflater = (LayoutInflater) context
                .getSystemService(Activity.LAYOUT_INFLATER_SERVICE);


        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.list_item_request, null);
            holder = new ViewHolder();

           holder.Accept=(Button)convertView.findViewById(R.id.accept);
            holder.Reject=(Button)convertView.findViewById(R.id.reject);
            holder.Email=(TextView)convertView.findViewById(R.id.Email);

            holder.Email.setText(rowItem);

            holder.Accept.setTag(position);
            holder.Reject.setTag(position);

            holder.Accept.setOnClickListener(this);
            holder.Reject.setOnClickListener(this);



            convertView.setTag(holder);

        } else
            holder = (ViewHolder) convertView.getTag();




        return convertView;





    }
    public void onClick(View v) {
        //Toast.makeText(context,"hOGAYA",Toast.LENGTH_LONG).show();
        if(v.getId()==R.id.accept){
            data=GrabbingData.get((int)v.getTag());
            Toast.makeText(context,"Accept : "+data,Toast.LENGTH_LONG).show();
            FirebaseDatabase.getInstance().getReference().child("EmailToId").child(reverseparser(data))
                    .addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            if(dataSnapshot.exists()){
                                UID= dataSnapshot.getValue().toString();
                                FirebaseDatabase.getInstance().getReference().child("UserPeers")
                                        .child(uid).addListenerForSingleValueEvent(new ValueEventListener() {
                                    @Override
                                    public void onDataChange(DataSnapshot dataSnapshot) {
                                        if(dataSnapshot.exists()){
                                            String IDs=dataSnapshot.getValue().toString();
                                            IDs=IDs+UID+" ";
                                            FirebaseDatabase.getInstance().getReference().child("UserPeers")
                                                    .child(uid).setValue(IDs);
                                            FirebaseDatabase.getInstance().getReference().child("UserFriends")
                                            .child(uid).setValue(delete(IDs,UID));
                                        }
                                    }

                                    @Override
                                    public void onCancelled(DatabaseError databaseError) {

                                    }
                                });
                            }
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {

                        }
                    });

        }
        else if(v.getId()==R.id.reject){
            String data=GrabbingData.get((int)v.getTag());
            Toast.makeText(context,"Reject : "+data,Toast.LENGTH_LONG).show();
            FirebaseDatabase.getInstance().getReference().child("EmailToId").child(reverseparser(data)).addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    if(dataSnapshot.exists()){
                        UID=dataSnapshot.getValue().toString();
                        FirebaseDatabase.getInstance().getReference().child("UserFriends").child(uid)
                        .addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(DataSnapshot dataSnapshot) {
                                if(dataSnapshot.exists()){
                                    String ids=dataSnapshot.getValue().toString();
                                    String newids=delete(ids,UID);
                                    FirebaseDatabase.getInstance().getReference().child("UserFriends")
                                            .child(uid).setValue(newids);
                                }
                            }

                            @Override
                            public void onCancelled(DatabaseError databaseError) {

                            }
                        });
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });

        }

    }
    public String reverseparser(String s){
        String email="";
        for(int i =0; i<s.length();i++){
            if(s.charAt(i)!='.'){
                email=email+String.valueOf(s.charAt(i));
            }
            else {
                email = email + '!';
            }
        }
        return email;
    }
    public String delete(String uids, String del) {
        String newids="";
        String returnids="";
        for(int i = 0 ; i<uids.length();i++) {
            if (uids.charAt(i) != ' ') {
                newids = newids + String.valueOf(uids.charAt(i));

            }
            else{
                if(newids.compareTo(del)!=0){
                    returnids=returnids+newids+" ";
                    newids="";

                }
                else{
                    newids="";
                }

            }
        }
        return returnids;

    }
}
