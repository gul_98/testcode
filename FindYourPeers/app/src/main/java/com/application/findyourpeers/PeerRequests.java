package com.application.findyourpeers;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class PeerRequests extends AppCompatActivity {
    ListView lv;
    RequestsAdapater adapter;
    String uid;
    String email;
    List<String> requestEmails;
    List<String> requestUid;
    ImageButton sendrequest;
    EditText requestEmail;
    String UID;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_peer_requests);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        lv=(ListView)findViewById(R.id.requests);
        requestEmails=new ArrayList<String>();
        requestUid=new ArrayList<String>();

        ///adding onlick and doing sending request
        SharedPreferences prefs = getSharedPreferences("UID", MODE_PRIVATE);
        uid = prefs.getString("uid", null);
        email=prefs.getString("email",null);
        adapter= new RequestsAdapater(this,R.layout.list_item_request,requestEmails);
        lv.setAdapter(adapter);
        FirebaseDatabase.getInstance().getReference().child("UserFriends").child(uid)
                .addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        if(dataSnapshot.exists()) {
                            Log.e("datasnapsot", dataSnapshot.getValue().toString());
                            requestUid = parser(dataSnapshot.getValue().toString());
                            Log.e("datasnapshot size", String.valueOf(requestUid.size()) );
                            if (requestUid.size()>1) {
                                for (int i = 0; i < requestUid.size(); i++) {
                                    Log.e("datasnapshot for loop", requestUid.get(i));
                                    FirebaseDatabase.getInstance().getReference().child("Users")
                                            .child(requestUid.get(i)).addListenerForSingleValueEvent(new ValueEventListener() {
                                        @Override
                                        public void onDataChange(DataSnapshot dataSnapshot) {
                                            requestEmails.add(dataSnapshot.getValue().toString());
                                            Log.e("datasnapshot firebase", dataSnapshot.getValue().toString());
                                            lv.notify();
                                            adapter.notifyDataSetChanged();
                                            adapter.notify();
                                        }

                                        @Override
                                        public void onCancelled(DatabaseError databaseError) {

                                        }
                                    });
                                }
                            }
                            else if(requestUid.size()==1 ){
                                Log.e("datasnapshot in " , "1");
                                FirebaseDatabase.getInstance().getReference().child("Users")
                                        .child(requestUid.get(0)).addListenerForSingleValueEvent(new ValueEventListener() {
                                    @Override
                                    public void onDataChange(DataSnapshot dataSnapshot) {
                                        requestEmails.add(reverseparser(dataSnapshot.getValue().toString()));
                                        Log.e("datasnapshot firebase", dataSnapshot.getValue().toString());

                                        adapter.notifyDataSetChanged();

                                    }

                                    @Override
                                    public void onCancelled(DatabaseError databaseError) {

                                    }
                                });


                            }
                            else{
                                Log.e("datasnapshot ", "else");
                                requestEmails.clear();
                                adapter.notifyDataSetChanged();
                            }
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });


    }
    public List<String> parser(String UIDS){
        String Uid="";
        Log.e("UIDS", UIDS);
        boolean oneid=true;
        List<String> uidlist= new ArrayList<String>();
        for (int i = 0; i < UIDS.length(); i++) {
            if (UIDS.charAt(i) != ' ') {
                Uid = Uid + String.valueOf(UIDS.charAt(i));
            } else {
                uidlist.add(Uid);
                Uid = "";
                oneid=false;
            }
        }


        return uidlist;
    }
    public String reverseparser(String s){
        String email="";
        for(int i =0; i<s.length();i++){
            if(s.charAt(i)!='!'){
                email=email+String.valueOf(s.charAt(i));
            }
            else{
                email=email+'.';
            }
        }
        return email;
    }


}